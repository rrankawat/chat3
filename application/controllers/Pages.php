<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

	public function index() {
		$data['page'] = "pages/login";
		$this->load->view("layout/web", $data);
	}

	public function register() {
		$data['page'] = "pages/register";
		$this->load->view("layout/web", $data);
	}

	public function chat() {
		if($this->session->flashdata('flash_message')) {
			$data['flashdata'] = $this->session->flashdata('flash_message'); 
		}
		
		$data['page'] = "pages/chat";
		$this->load->view("layout/web", $data);
	}
}
