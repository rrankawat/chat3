<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	public function register() {
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[6]|callback_check_username_exists');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('password2', 'Password Confirmation', 'trim|required|min_length[8]|matches[password]');

		if($this->form_validation->run() === FALSE) {
			redirect('register');
		} else {
			// Enc password
			$enc_password = md5($this->input->post('password'));
			$this->user_model->register($enc_password);

			// Set message
			$alerts = array(
				'type' => 'success',
				'message' => 'You are now registerd'
			);
			$this->session->set_flashdata('flash_message', $alerts);
			redirect('chat');
		}
	}

	// Check if username is exists
	public function check_username_exists($username) {
		$this->form_validation->set_message('check_username_exists', 'This username already been taken.');
		if($this->user_model->check_username_exists($username)) {
			return true;
		} else {
			return false;
		}
	}

}
