<?php

if(!empty($page)) {	
	$this->load->view('template/header');
	$this->load->view($page);
	$this->load->view('template/footer');
}
else {
	echo "404";
}