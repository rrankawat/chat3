<br/>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2 class="text-primary">Sign In</h2>
            <br/>
            <?php 
                $attributes = array(
                  'id' => 'loginForm',
                  'name' => 'loginForm'
                );
                echo form_open('users/login', $attributes);
            ?>
                <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <input type="text" class="form-control" name="username" placeholder="Username">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <br/><br/>
                <span>New user ? <a href="<?php echo base_url('register'); ?>">Create Account</a></span>
            </form>
        </div>
    </div>
</div>