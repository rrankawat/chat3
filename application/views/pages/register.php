<br/>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2 class="text-primary">Create New Account</h2>
            <br/>
            <?php 
                $attributes = array(
                  'id' => 'registerForm',
                  'name' => 'registerForm'
                );
                echo form_open('users/register', $attributes);
            ?>
                <div class="form-group">
                    <label for="exampleInputEmail1">Username</label>
                    <input type="text" class="form-control" name="username" placeholder="Username">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" id="password" class="form-control" name="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Re-type Password</label>
                    <input type="password" class="form-control" name="password2" placeholder="Re-type Password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <br/><br/>
                <span>Aready have a account <a href="<?php echo base_url('index'); ?>">Sign In</a></span>
            </form>
        </div>
    </div>
</div>