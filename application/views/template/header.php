<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>My Chat</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">

    <!-- custom -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/custom.css'); ?>">

    <!-- emoji -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.1/emojionearea.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.1/emojionearea.min.js"></script>

    <style type="text/css">
        .emojionearea .emojionearea-editor {
            min-height: 4em;
        }
        .error {
            color: #dc3545;
        }
        .chat {
            height: 610px;
        }
    </style>
</head>

<body>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>

    <div class="content container-fluid bootstrap snippets">